package exercitiu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

//varianta cu if-uri
public class Translator {
	
	private static void getDataFromLine(String line, PrintStream writer) {
		
		//8-11 area A 12-72 area B
		//cand se continua o linie e intotdeauna - sau ne luam dupa . ?
		//se poate si fara PIC dar nu clasa?
		switch (line.charAt(6)) {
		    
			case ('*') : {
				writer.println("comment");
				break;
			}
			
			case ('D') : {
				writer.println("debug-mode");
				break;
			}
			
			case ('-') : {
				writer.println("continuation");
				break;
			}
			
			default : {
				
				line = line.substring(7);
				String[] wordsPrev = line.trim().split("\\s+");
								
				if (!line.contains("PIC") && line.charAt(0) != ' ')
					writer.println("class");
			
				else 
					for (int i = 0; i < wordsPrev.length; i++) {
						    if (i >= 1 && wordsPrev[i-1].equals("PIC")) {
						    	
								if (wordsPrev[i].contains("X"))
									   writer.println("String");
								
								else if (wordsPrev[i].contains("A")) 
									   writer.println("String");
								
								else if (wordsPrev[i].contains("9")) {
									
									if (wordsPrev[i].contains("V"))
									   writer.println("Float");
									
									else
								       writer.println("Int");
								}
								
								else 
								   writer.println("OTHER");								
	                        }
					}
		    }
		}		
	}

	public static void copybookToJavaTypes(String copybookPath)
	{
				
		try (Scanner copybookScanner = new Scanner(
				                          new File(
				                        	 "C:\\Users\\GeorgianaArazan\\Desktop\\hospitalCopybook.txt"));
			 PrintStream javaWriter = new PrintStream(
					                      new FileOutputStream(
					                         "C:\\Users\\GeorgianaArazan\\Desktop\\hospitalJava.txt"))) {
			       
			String line = "";
			String previousLine;
			
			while (copybookScanner.hasNextLine()) {
				
				previousLine = line;
				line = copybookScanner.nextLine();
				//ultimul cuv din prev line nu are punct -> se concateneaza prev cu line
				//ultimul nu e punct! e \n poate
				if (line.contains("01"))
				System.out.println(line.charAt(line.length() - 1));
				//pune pct numa daca CRLF imediat langa . -> mai bine aflu din ult cuvant
	//			                                          -> concatenare pe cond ult cuvant
//				if (line.charAt(line.length() - 1) != '.')
//					line = previousLine + line;
//				if (!line.contains("REDEFINES") && previousLine != "") 
//					getDataFromLine(previousLine, javaWriter);
//									
//				if (!copybookScanner.hasNextLine()) 
//					getDataFromLine(line, javaWriter);				
			}
		}
	 
    	catch (IOException e) {
    		System.out.println(e.toString());
    	}
    
    }

	public static void main(String[] args) {
		
		copybookToJavaTypes("");
	}

}
